# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the CatalogRef model in thredds-control-center models
-------------------------------------------
"""
from __future__ import annotations

from typing import TYPE_CHECKING, Callable

from tds_control import models
from tds_control.tasks import restart_server

if TYPE_CHECKING:
    import requests


def test_catalogref_creation_and_server_response(
    tds_rf: Callable[[str], requests.Response], tdsserver: models.TDSServer
):
    """Test creating a CatalogRef instance and verifying server response."""
    # create a TDSCatalog instance as it's a foreign key in CatalogRef
    tds_catalog_first = models.TDSCatalog.objects.create(
        name="test_tdscatalog_first"
    )
    tds_catalog_second = models.TDSCatalog.objects.create(
        name="test_tdscatalog_second"
    )

    # Create a CatalogRef instance
    catalog_ref = models.CatalogRef.objects.create(
        conn_catalog=tds_catalog_second,
        catalog=tds_catalog_first,
        title="Test CatalogRef",
    )

    # Add the TDSCatalog to the TDSServer
    tdsserver.catalogs.add(tds_catalog_first)
    tdsserver.catalogs.add(tds_catalog_second)

    # Restart the server to apply changes
    restart_server()

    # Test the server response
    response = tds_rf(
        f"thredds/catalog/catalogs/{tds_catalog_first.name}.html"
    )
    print(response.reason)
    assert response.status_code == 200

    # check the attributes of the CatalogRef instance
    assert catalog_ref.title == "Test CatalogRef"
    assert catalog_ref.conn_catalog == tds_catalog_second
    assert catalog_ref.catalog == tds_catalog_first
    assert str(catalog_ref.uuid) == str(catalog_ref)


def test_catalogref_string_representation(tdsserver: models.TDSServer):
    """Test the string representation of CatalogRef."""
    tds_catalog_first = models.TDSCatalog.objects.create(
        name="test_tdscatalog_first"
    )
    tds_catalog_second = models.TDSCatalog.objects.create(
        name="test_tdscatalog_second"
    )
    # Add the TDSCatalog to the TDSServer
    tdsserver.catalogs.add(tds_catalog_first)
    tdsserver.catalogs.add(tds_catalog_second)
    # Create a CatalogRef instance
    catalog_ref = models.CatalogRef.objects.create(
        conn_catalog=tds_catalog_second,
        catalog=tds_catalog_first,
        title="Test CatalogRef",
    )

    assert str(catalog_ref) == str(catalog_ref.uuid)
