# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

# docker-compose setup for running the thredds-control-center test suite
#
# first spin up a django server that runs the migrations and populates the
# XML-files for the THREDDS server, then start the thredds server, then run the
# test suite.
#
# Run this via::
#
#     docker compose -f docker-compose.test.yml up --exit-code-from django-test
version: "3.7"

volumes:
  thredds-data:
  pgdata:

x-tomcat-env: &tomcat-env
  TDS_MANAGER_USER: "${TDS_MANAGER_USER:-manager}"
  TDS_MANAGER_PASSWORD: "${TDS_MANAGER_PASSWORD:-changeme}"
  TDS_CONTENT_ROOT_PATH: "${TDS_CONTENT_ROOT_PATH:-/usr/local/tomcat/content/}"

x-pg-env: &pg-env
  POSTGRESQL_PASSWORD: "${POSTGRES_PASSWORD:-changeme}"
  POSTGRESQL_USER: "${POSTGRES_USER:-django_user}"
  POSTGRESQL_DATABASE: "${POSTGRES_DB:-django_db}"

x-django-env: &django-env
  <<: [*pg-env, *tomcat-env]
  TDS_TOMCAT_URL: "http://${TDS_HOST:-thredds}:8080"
  DATABASE_SERVICE_NAME: postgresql
  POSTGRESQL_SERVICE_HOST: postgres
  DATABASE_ENGINE: postgresql


services:

  postgres:
    image: "quay.io/sclorg/postgresql-13-c9s"
    stop_grace_period: "3s"
    expose:
      - 5432
    volumes:
      - "pgdata:/var/lib/pgsql/data"
    environment:
      <<: *pg-env
      POSTGRES_HOST_AUTH_METHOD: trust
    healthcheck:
      test: pg_isready -d ${POSTGRES_DB:-django_db} -U ${POSTGRES_USER:-django_user}
      interval: 10s
      retries: 5
      start_period: 10s
      timeout: 5s

  django:
    # background django process to populate initial xml files
    build:
      context: "./"
      dockerfile: "./docker/django/Dockerfile"
    image: "${DJANGO_IMAGE:-thredds-control-center-django}"
    hostname: "${DJANGO_HOST:-django}"
    environment:
      <<: *django-env
      ALLOWED_HOSTS: "0.0.0.0,localhost,127.0.0.1"
    volumes:
      - "thredds-data:${TDS_CONTENT_ROOT_PATH:-/usr/local/tomcat/content/}"
    ports:
      - "8002:8080"
    expose:
      - "8080"
    depends_on:
      postgres:
        condition: service_healthy
    healthcheck:
      test: curl --fail http://localhost:8080/admin/
      interval: 10s
      retries: 5
      start_period: 10s
      timeout: 5s

  thredds:
    # thredds server for test setup
    build: "./docker/thredds/"
    hostname: "${TDS_HOST:-thredds}"
    healthcheck:
      test: curl --fail http://localhost:8080/thredds/catalog/catalog.html
      interval: 10s
      retries: 10
      start_period: 90s
      timeout: 5s
    ports:
      - "8001:8080"
    expose:
      - "8080"
    volumes:
      - "thredds-data:${TDS_CONTENT_ROOT_PATH:-/usr/local/tomcat/content/}"
    environment:
      <<: *tomcat-env
      ALLOWED_MANAGER_HOSTS: >-
        ${DJANGO_HOST:-django},
        ${TDS_HOST:-thredds},
        ${DJANGO_Q_HOST:-django-q},
        ${DJANGO_TEST_HOST:-django-test}
      THREDDS_XMX_SIZE: 2G
      THREDDS_XMS_SIZE: 1G
      TOMCAT_USER_ID: 1001
      TOMCAT_GROUP_ID: 1001
    depends_on:
      django:
        condition: service_healthy
      django-q:
        condition: service_started

  django-test:
    # run the test suite
    image: "${DJANGO_IMAGE:-thredds-control-center-django}"
    hostname: "${DJANGO_TEST_HOST:-django-test}"
    environment:
      <<: *django-env
    volumes:
      - "thredds-data:${TDS_CONTENT_ROOT_PATH:-/usr/local/tomcat/content/}"
    command: [make, quick-test]
    depends_on:
      thredds:
        condition: service_healthy

  django-q:
    # run the test suite
    image: "${DJANGO_IMAGE:-thredds-control-center-django}"
    hostname: "${DJANGO_Q_HOST:-django-q}"
    environment:
      <<: *django-env
    volumes:
      - "thredds-data:${TDS_CONTENT_ROOT_PATH:-/usr/local/tomcat/content/}"
    command: [python, manage.py, qcluster]
    depends_on:
      django:
        condition: service_healthy
